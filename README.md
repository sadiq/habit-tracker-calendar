Habit tracker calendar helps you create printable
minimalistic calendars to track your habit progress.
You can either build it your own, ore use the pre-built
PDFs in the `output` directory.

In Kerala calendar, the week start from Sunday, and has
all holidays specific to [Kerala](https://en.wikipedia.org/wiki/Kerala)
highlighted as red.

In World calendar, the week start from Monday, and has
all Saturdays and Sundays highlighted as red.

Author: Mohammed Sadiq: [https://www.sadiqpk.org](https://www.sadiqpk.org)

Source Repository: [SourceHut](https://sr.ht/~sadiq/habit-tracker-calendar)


## Build requirements

To build the project, you shall need:

- LuaLaTeX and related LaTeX packages
- GNU make


## How to build

* `make 2024-kerala # Build the complete set of 2024 Kerala calendars`
* `make 2024-world # Build the complete set of 2024 World calendars`
* `make YEAR=2024 MONTH=04 # Kerala and Worldwide 3 month calendar starting from 2024 April`
* `make YEAR=2024 MONTH=04 kerala # Kerala specific 3 month calendar starting from 2024 April`

The generated PDF files are stored in the `output` directory.


## How I use it

- Print as many calendars as needed (I combined ~25 copies of a calendar (1 calendar length = ~12cm)
  on an A3 paper and printed it out).
  - Use some light grey for print cut marks
- Add the habit I want to track - one calendar per habit.
  - Eg.: Read 10 pages, Exercise for 30 minutes, Wake at 5:00, etc.
- Mark columns for each successfully completed day (with a CD Marker or highlighter or so)
  - If using a highligher, different colors can be used to track it better. Say, green if done
    right.  Orange, if started, but incomplete.  Red if not done, etc.
- I stick my calendars on my whiteboard using a magnet.


## License

Habit tracker calendar has been released into public domain.
Feel free to do what ever you want.  Attribution is recommended
but not required.

If you want to thank me, you may:

- Use and promote [free software](https://en.wikipedia.org/wiki/Free_software).
- Switch to some [GNU/Linux](https://getgnulinux.org).
- Regardless of the Operating System you use, always buy
  Debian main compatible hardware or at least ask for one.
  - This can help free software, improve hardware lifetime and reduce
    [Planned obsolescence](https://en.wikipedia.org/wiki/Planned_obsolescence).
