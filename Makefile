YEAR ?= 2024
MONTH ?= 1
END = $(shell echo $$(( $(MONTH) + 2 )))

ifeq ($(shell test $(MONTH) -lt 10; echo $$?),0)
  MONTH := $(shell echo 0$(MONTH))
endif

ifeq ($(shell test $(END) -lt 10; echo $$?),0)
  END := $(shell echo 0$(END))
endif


all: kerala world

2025-kerala:
	make YEAR=2025 MONTH=01 kerala
	make YEAR=2025 MONTH=04 kerala
	make YEAR=2025 MONTH=07 kerala
	make YEAR=2025 MONTH=10 kerala

2025-world:
	make YEAR=2025 MONTH=01 world
	make YEAR=2025 MONTH=04 world
	make YEAR=2025 MONTH=07 world
	make YEAR=2025 MONTH=10 world

world:
	mkdir -p output
	lualatex --jobname world-$(YEAR)-$(MONTH)-$(END) --output-directory=output \
		"\def\yearnow{$(YEAR)} \def\monthnow{$(MONTH)} \input{world.tex}"

kerala:
	mkdir -p output
	lualatex --jobname kerala-$(YEAR)-$(MONTH)-$(END) --output-directory=output \
		"\def\yearnow{$(YEAR)} \def\monthnow{$(MONTH)} \input{kerala.tex}"

clean:
	mkdir -p output
	(cd output && rm -rf *.log *.aux *.out)
	rmdir --ignore-fail-on-non-empty output
	rm -rf *.log *.aux *.out
